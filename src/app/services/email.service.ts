import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, share } from 'rxjs/operators';
import { Email } from '../app-model.global';
import { SingletonService } from './singleton.service';

@Injectable()
export class EmailService {
    private allEmails: Email[];

    constructor(private http: HttpClient, public singletonService: SingletonService) {}

    getAllEmails(): Observable<any[]> {
        return this.http.get<Email[]>('./assets/email.json').pipe(catchError(this.serverError), share());
    }

    getEmail(id: any): Promise<Email> {
        return new Promise((resolve, reject) => {
            this.singletonService.emails$.subscribe((all) => (this.allEmails = all));
            setTimeout(() => {
                var result = this.allEmails.find((email) => {
                    email.id === +id;
                });
                if (result) {
                    resolve(result);
                } else resolve(new Email());
            }, 1000);
        });
    }

    serverError(err: any) {
        console.log('sever error:', err);
        if (err instanceof Response) {
            return throwError(err.json() || 'backend server error');
        }
        return throwError(err || 'backend server error');
    }
}
