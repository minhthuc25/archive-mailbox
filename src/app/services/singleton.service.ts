import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Email } from '../app-model.global';

@Injectable()
export class SingletonService {
    private globalStorage = new BehaviorSubject<Email[]>(this.getGlobal());
    emails$: Observable<Email[]> = this.globalStorage.asObservable();
    g_emails: Email[];

    constructor() {
        this.g_emails = [];
    }

    setGlobal(val: Email[]) {
        this.globalStorage.next(val);
    }

    private getGlobal(): Email[] {
        if (this.g_emails) {
            return this.g_emails;
        } else {
            return [];
        }
    }
}
