import { Component, OnInit } from '@angular/core';
import { EmailService } from '../services/email.service';
import { Email, FilterObject } from '../app-model.global';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { SingletonService } from '../services/singleton.service';

@Component({
    selector: 'app-archive',
    templateUrl: './archive.component.html',
    styleUrls: ['./archive.component.scss'],
})
export class ArchiveComponent implements OnInit {
    allEmail: Email[] = [];
    allFilteredEmail: Email[] = [];
    collectionSize: number;
    currentSortCol: string = '';
    currentFilter: FilterObject;
    filter: FormGroup;
    page: number;
    pageSize: number;
    reverse = true;
    searchType: string = 'all';

    private _displayEmails: Email[];

    regexpDate = new RegExp(/^\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/);

    constructor(
        public emailService: EmailService,
        public singletonService: SingletonService,
        private fb: FormBuilder,
        private router: Router
    ) {}

    ngOnInit() {
        this.collectionSize = 100;
        this.page = 1;
        this.pageSize = 25;
        this.currentFilter = new FilterObject();
        this.currentFilter.field = 'all';
        this.currentFilter.value = '';

        this.filter = this.fb.group({
            value: [''],
            field: [''],
        });

        this.emailService.getAllEmails().subscribe((response) => {
            this.allEmail = response.map((items, index) => {
                items.id = index;
                return items;
            });
            this.allFilteredEmail = this.allEmail;
            this.singletonService.setGlobal(this.allEmail);
            this.collectionSize = response.length;
        });
    }

    get displayEmail(): Email[] {
        this._displayEmails = this.allFilteredEmail.slice(
            (this.page - 1) * this.pageSize,
            (this.page - 1) * this.pageSize + this.pageSize
        );
        return this._displayEmails;
    }

    set displayEmail(input: Email[]) {
        this._displayEmails = input;
    }

    openEmail(email: Email) {
        this.router.navigate(['/archive/' + email.id]);
    }

    sortBy(col: string) {
        if (col === this.currentSortCol) {
            this.reverse = !this.reverse;
        } else {
            this.currentSortCol = col;
        }
        switch (col) {
            case 'id':
                if (this.reverse) {
                    this._displayEmails = this.allFilteredEmail.sort((a, b) => a.id - b.id);
                } else {
                    this._displayEmails = this.allFilteredEmail.sort((a, b) => b.id - a.id);
                }

                break;
            case 'from':
                if (this.reverse) {
                    this._displayEmails = this.allFilteredEmail.sort((a, b) => a.from.localeCompare(b.from));
                } else {
                    this._displayEmails = this.allFilteredEmail.sort((a, b) => b.from.localeCompare(a.from));
                }

                break;
            case 'subject':
                if (this.reverse) {
                    this._displayEmails = this.allFilteredEmail.sort((a, b) => a.subject.localeCompare(b.subject));
                } else {
                    this._displayEmails = this.allFilteredEmail.sort((a, b) => b.subject.localeCompare(a.subject));
                }

                break;
            case 'date':
                if (this.reverse) {
                    this._displayEmails = this.allFilteredEmail.sort(
                        (a, b) => new Date(a.date).getTime() - new Date(b.date).getTime()
                    );
                } else {
                    this._displayEmails = this.allFilteredEmail.sort(
                        (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
                    );
                }

                break;
            default:
                break;
        }
    }

    onFilterValueChange() {
        if (this.currentFilter.value == '') {
            this.allFilteredEmail = this.allEmail;
        } else {
            this.allFilteredEmail = [];
            var searchValue = this.currentFilter.value.toLowerCase();
            switch (this.currentFilter.field) {
                case 'all':
                    if (this.regexpDate.test(searchValue)) {
                        var datesplit = searchValue.split('-');
                        var year = datesplit[0];
                        var month = datesplit[1];
                        var day = datesplit[2];
                        console.log(datesplit);
                        this.allEmail.forEach((item) => {
                            item.date = new Date(item.date);
                            if (
                                item.date.getUTCFullYear() == +year &&
                                item.date.getUTCMonth() == +month - 1 &&
                                item.date.getUTCDate() == +day
                            ) {
                                this.allFilteredEmail.push(item);
                            }
                        });
                    } else {
                        this.allEmail.forEach((item) => {
                            if (
                                item.from.toLowerCase().includes(searchValue) ||
                                item.subject.toLowerCase().includes(searchValue) ||
                                item.body.toLowerCase().includes(searchValue)
                            ) {
                                this.allFilteredEmail.push(item);
                            }
                            if (item.to && item.to.length > 0) {
                                for (var i = 0; i < item.to.length; i++) {
                                    if (item.to[i].toLowerCase().includes(searchValue)) {
                                        this.allFilteredEmail.push(item);
                                        break;
                                    } else continue;
                                }
                            }
                        });
                    }
                    break;
                case 'sendby':
                    this.allEmail.forEach((item) => {
                        if (item.from.toLowerCase().includes(searchValue)) {
                            this.allFilteredEmail.push(item);
                        }
                    });
                    break;
                case 'sendto':
                    this.allEmail.forEach((item) => {
                        if (item.to && item.to.length > 0) {
                            for (var i = 0; i < item.to.length; i++) {
                                if (item.to[i].toLowerCase().includes(searchValue)) {
                                    this.allFilteredEmail.push(item);
                                    break;
                                } else continue;
                            }
                        }
                    });
                    break;
                case 'date':
                    if (this.regexpDate.test(searchValue)) {
                        var datesplit = this.currentFilter.value.split('-');
                        var year = datesplit[0];
                        var month = datesplit[1];
                        var day = datesplit[2];
                        console.log(datesplit);
                        this.allEmail.forEach((item) => {
                            item.date = new Date(item.date);
                            if (
                                item.date.getUTCFullYear() == +year &&
                                item.date.getUTCMonth() == +month - 1 &&
                                item.date.getUTCDate() == +day
                            ) {
                                this.allFilteredEmail.push(item);
                            }
                        });
                    }
                    break;
            }
            this.collectionSize = this.allFilteredEmail.length;
        }
    }
}
