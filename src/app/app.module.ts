import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArchiveComponent } from './archive/archive.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ViewMailComponent } from './view-mail/view-mail.component';

import { EmailService } from './services/email.service';
import { SingletonService } from './services/singleton.service';

import { NgbModule, NgbPaginationModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [AppComponent, NavigationComponent, ArchiveComponent, ViewMailComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NgbPaginationModule,
        NgbAlertModule,
    ],
    providers: [EmailService, SingletonService],
    bootstrap: [AppComponent],
})
export class AppModule {}
