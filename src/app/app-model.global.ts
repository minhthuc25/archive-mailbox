export class Email {
    id: number;
    from: string;
    to: string[];
    cc: string[];
    bcc: string[];
    subject: string;
    body: string;
    date: Date;

    constructor() {
        this.date = new Date();
    }
}

export class FilterObject {
    field: string;
    value: string;
}