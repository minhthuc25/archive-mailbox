import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationComponent } from './navigation/navigation.component';
import { ArchiveComponent } from './archive/archive.component';
import { ViewMailComponent } from './view-mail/view-mail.component';

const routes: Routes = [
    { path: '', redirectTo: 'archive', pathMatch: 'full' },
    { path: 'archive', component: ArchiveComponent, data: { title: 'Archive' } },
    { path: 'archive/:id', component: ViewMailComponent, data: { title: 'Archive' } },
    { path: '**', redirectTo: 'archive', pathMatch: 'full' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            useHash: false,
            anchorScrolling: 'enabled',
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule {}
