import { Component } from '@angular/core';
import { Email } from '../app-model.global';
import { EmailService } from '../services/email.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { SingletonService } from '../services/singleton.service';
import { identifierModuleUrl } from '@angular/compiler';

@Component({
    selector: 'app-view-mail',
    templateUrl: './view-mail.component.html',
    styleUrls: ['./view-mail.component.scss'],
})
export class ViewMailComponent {
    selectedId = -1;
    selectedEmail = new Email();
    allEmails: Email[];
    previousEmails: Email[];
    followingEmails: Email[];
    toggle = {};
    toggle2 = {};

    constructor(private route: ActivatedRoute, private router: Router, private singletonService: SingletonService) {
        this.allEmails = [];
        this.previousEmails = [];
        this.followingEmails = [];
    }

    ngOnInit() {
        this.singletonService.emails$.subscribe((res) => (this.allEmails = res));
        this.selectedId = +this.route.snapshot.paramMap.get('id');

        if (this.selectedId <= -1 || this.allEmails.length == 0) {
            this.router.navigate(['/archive']);
        } else {
            this.selectedEmail = this.allEmails.find((x) => x.id === this.selectedId);
            this.detectTrailing();
        }
    }

    back() {
        this.router.navigate(['/archive']);
    }

    detectTrailing() {
        var searchterm = [];
        this.followingEmails = [];
        this.previousEmails = [];

        if (this.selectedEmail.body.includes('')) {
            var temp = this.selectedEmail.body.split('\n');
            temp.forEach((i) => {
                if (i.includes('Subject:')) {
                    var tempstring = i.replace('Subject:', '').trim();
                    if (tempstring.includes('FW: ')) {
                        tempstring = tempstring.replace('FW: ', '').trim();
                    }
                    if (tempstring.includes('RE: ')) {
                        tempstring = tempstring.replace('RE: ', '').trim();
                    }
                    searchterm.push(tempstring);
                }
            });
        }

        this.allEmails.forEach((element) => {
            searchterm.forEach((i) => {
                if (
                    (element.subject === 'FW: ' + i || element.subject === 'RE: ' + i || element.subject === i) &&
                    element.id !== this.selectedEmail.id
                ) {
                    if (new Date(element.date).getTime() - new Date(this.selectedEmail.date).getTime()) {
                        this.followingEmails.push(element);
                    } else {
                        this.previousEmails.push(element);
                    }
                }
            });
        });

        if (this.followingEmails.length > 0) {
            this.followingEmails = this.followingEmails.sort(
                (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
            );
        }

        if (this.previousEmails.length > 0) {
            this.previousEmails = this.previousEmails.sort(
                (a, b) => new Date(b.date).getTime() - new Date(a.date).getTime()
            );
        }
    }
}
